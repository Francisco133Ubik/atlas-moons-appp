//
//  AppStoreReviewManager.swift
//  Atlas Moons
//
//  Created by Francisco Javier Delgado García on 17/10/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//


import StoreKit


enum AppStoreReviewManager {
    static let minimumReviewWorthyActionCount = 5
    
    static func requestReviewIfAppropriate() {
        let defaults = UserDefaults.standard
        
        var actionCount = defaults.integer(forKey: "reviewWorthyActionCount")
        actionCount += 1
        defaults.set(actionCount, forKey: "reviewWorthyActionCount")
        
        print(actionCount)
        
        guard actionCount >= minimumReviewWorthyActionCount else {
            return
        }
        
        SKStoreReviewController.requestReview()
        defaults.set(0, forKey: "reviewWorthyActionCount")
    }
}
