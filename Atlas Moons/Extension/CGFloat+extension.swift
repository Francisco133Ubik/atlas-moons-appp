//
//  CGFloat+extension.swift
//  Atlas Moons
//
//  Created by Francisco Javier Delgado García on 26/01/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit

extension CGFloat{
    var dp: CGFloat{
        
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        switch (deviceIdiom) {
        case .pad:
            return (self / 450) * UIScreen.main.bounds.width
        case .phone:
            return (self / 320) * UIScreen.main.bounds.width
        default:
            return self * 1
        }
        
        
    }
}
