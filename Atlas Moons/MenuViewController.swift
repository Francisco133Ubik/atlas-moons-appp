//
//  MenuViewController.swift
//  Atlas Moons
//
//  Created by Francisco Javier Delgado García on 23/06/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit

enum StateMenu {
    case menu
    case detail
}

class MenuViewController: UIViewController {
    @IBOutlet weak var visualEffectViewBackground: UIVisualEffectView!{didSet{
        visualEffectViewBackground.alpha = 0
        }}
    @IBOutlet weak var buttonClose: UIButton!{didSet{
        buttonClose.alpha = 0
        }}
    let buttonHome = UIView.buttonMenu(withTitle: "HOME")
    let buttonAbout = UIView.buttonMenu(withTitle: "ABOUT")
    let buttonContact = UIView.buttonMenu(withTitle: "CONTACT")
    @IBOutlet weak var textViewInfo: UITextView!
    let aboutInfo = """


    THE ATLAS OF MOONS



    For this survey, the term "major moon" denotes Phobos and Deimos as well as bodies that are large enough to form a spheroid. Moons orbiting dwarf planets Eris, Haumea, and Makemake are excluded.



    App inspired by the interactive page for National Geographic: https://www.nationalgeographic.com/science/2019/07/the-atlas-of-moons/



    Published July 9, 2019.
    """
    
    
    let contactInfo = """


    CONTACT ME



    If you have comments, questions or feedback about my work — you’re more than welcome to contact me.



    Email: franciscojdelgadog@gmail.com

    GitLab: https://gitlab.com/Francisco133Ubik


    """
    var state = StateMenu.menu
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView(){
        textViewInfo.dataDetectorTypes = .all
        textViewInfo.prepareForAnimation(position: .bottom)
        view.add(buttonHome) {
            $0.prepareForAnimation(position: .right)
            $0.addTarget(self, action: #selector(tapButtonHome), for: .touchUpInside)
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor,
                                            constant: 30),
                $0.centerYAnchor.constraint(equalTo: view.centerYAnchor,
                                            constant: -CGFloat(100).dp),
                $0.widthAnchor.constraint(equalToConstant: CGFloat(100).dp)
            ])
        }
        view.add(buttonAbout) {
            $0.prepareForAnimation(position: .right)
            $0.addTarget(self, action: #selector(tapAbout), for: .touchUpInside)
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: buttonHome.leadingAnchor),
                $0.widthAnchor.constraint(equalToConstant: CGFloat(100).dp),
                $0.topAnchor.constraint(equalTo: buttonHome.bottomAnchor,
                                        constant: CGFloat(60).dp)
            ])
        }
        view.add(buttonContact) {
            $0.prepareForAnimation(position: .right)
            $0.addTarget(self, action: #selector(tapContact), for: .touchUpInside)
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: buttonHome.leadingAnchor),
                $0.widthAnchor.constraint(equalToConstant: CGFloat(100).dp),
                $0.topAnchor.constraint(equalTo: buttonAbout.bottomAnchor,
                                        constant: CGFloat(60).dp)
            ])
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateView()
    }
    
    private func animateView(){
        let animation = UIViewPropertyAnimator(duration: 0.35, dampingRatio: 0.9) {
            self.visualEffectViewBackground.alpha = 0.8
        }
        animation.startAnimation()
        buttonClose.animateShow(delay: 0.35)
        buttonHome.animateShow(delay: 0.2)
        buttonAbout.animateShow(delay: 0.3)
        buttonContact.animateShow(delay: 0.4)
    }
    
    @IBAction func dismissView() {
        switch state {
        case .menu:
            animateDismissView()
        case .detail:
            state = .menu
            animateShowMenu(show: true)
        }
    }
    
    
    private func animateDismissView(){
        (presentingViewController as? HomeViewController)?.openMenu()
        UIView.animate(withDuration: 0.35, animations: {
            self.view.alpha = 0
        }) { (_) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    
    @objc func tapButtonHome(){
        animateDismissView()
    }
    
    @objc func tapAbout(){
        state = .detail
        textViewInfo.text = aboutInfo
        animateShowMenu(show: false)
        
    }
    
    @objc func tapContact(){
        state = .detail
        textViewInfo.text = contactInfo
        animateShowMenu(show: false)
    }
    
    
    private func animateShowMenu(show: Bool){
        animateButtonMenu(withImage: show ? UIImage(named: "icon_close") : UIImage(named: "iconBack"))
        if show {
            UIView.animate(withDuration: 0.35, animations: {
                self.textViewInfo.alpha = 0
                self.textViewInfo.transform = CGAffineTransform(translationX: 0, y: 20)
            }) { _ in
                let animation = UIViewPropertyAnimator(duration: 0.35, dampingRatio: 0.9) {
                    self.buttonHome.alpha = 1
                    self.buttonHome.transform = .identity
                    self.buttonAbout.alpha =  1
                    self.buttonAbout.transform = .identity
                    self.buttonContact.alpha = 1
                    self.buttonContact.transform =  .identity
                }
                animation.startAnimation()
            }
        }else{
            let animation = UIViewPropertyAnimator(duration: 0.35, dampingRatio: 0.9) {
                self.buttonHome.alpha = 0
                self.buttonHome.transform =  CGAffineTransform(translationX: -10, y: 0)
                self.buttonAbout.alpha =  0
                self.buttonAbout.transform =  CGAffineTransform(translationX: -10, y: 0)
                self.buttonContact.alpha =  0
                self.buttonContact.transform =  CGAffineTransform(translationX: -10, y: 0)
            }
            animation.startAnimation()
            animation.addCompletion { _ in
                UIView.animate(withDuration: 0.35) {
                    self.textViewInfo.alpha = 1
                    self.textViewInfo.transform = .identity
                }
            }
        }
    }
    
    private func animateButtonMenu(withImage image: UIImage?){
        UIView.transition(with: buttonClose,
                          duration: 0.35,
                          options: .transitionFlipFromTop,
                          animations: { self.buttonClose.setImage(image, for: . normal) },
                          completion: nil)
    }
}



    
